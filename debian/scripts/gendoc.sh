#!/usr/bin/env bash
# This script creates the API documentation files.
LAST_CHANGE=$(dpkg-parsechangelog -S Date)
BUILD_DATE=$(LC_ALL=C date -u "+%B %d, %Y" -d "$LAST_CHANGE")
SPHINXOPTS="-D html_last_updated_fmt=\"$BUILD_DATE\""

echo "Creating API documentation"

if [ -d docs ]; then
    cd docs/sphinx; make html SPHINXOPTS="$SPHINXOPTS"
else
    exit 0
fi
