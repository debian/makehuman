#!/usr/bin/env bash

echo "Building assets"
cd makehuman/
python download_assets.py
./cleannpz.sh
./cleanpyc.sh
python compile_targets.py
python compile_models.py
find . -type f -iname \*.target -exec rm -rf {} \;
find . -type f -iname \*.obj -exec rm -rf {} \;
#python compile_proxies.py
exit 0
